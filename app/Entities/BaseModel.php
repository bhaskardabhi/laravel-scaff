<?php namespace App\Entities;

use Eloquent;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model{

  public function add($fillableArray = [], $nonFillableArray = [])
  {
    $this->fill($fillableArray);

    foreach ($nonFillableArray as $key => $value)
    {
      $this->{$key} = $value;
    }
    
    $this->save();

    return $this;
  }

  public function bulkInsert($data)
  {
    $time = Carbon::now();

    foreach ($data as &$currentData)
    {
      $currentData['created_at'] = $time;
      $currentData['updated_at'] = $time;
    }

    return $this->insert($data);
  }
  
}
