<!doctype html>
<html class="no-js" lang="">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('pageTitle', 'Project Name')</title>
  <meta name="description" content="">
  <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  @yield('css')
</head>
<body>
  @include('partials.navbar')
  <!--[if lt IE 8]>
    <div class="container"><div class="row"><div class="col-md-12">
    <div class="alert alert-danger" role="alert"><p>You are using an <strong>outdated</strong> browser. Please <a class="alert-link" href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p></div></div></div>
    </div>
  <![endif]-->

  @yield('content')

  <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  @yield('scripts')
</body>
</html>
